package com.devcamp.dailycampaign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.dailycampaign.service.DateService;

@CrossOrigin
@RestController
public class CDailyCampaign {

    @Autowired
    private DateService dateService;

	@GetMapping("/devcamp-date")
	public String getDateViet(@RequestParam(value = "username", defaultValue = "Pizza Lover") String name) {
		return dateService.getLoiChaoDate(name);
	}
    
}
