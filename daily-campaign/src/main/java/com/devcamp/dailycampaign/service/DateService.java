package com.devcamp.dailycampaign.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.stereotype.Service;

@Service
public class DateService {

    // phương thức trả về văn bản chứa thứ ngày 

    public String getLoiChaoDate(String paramName){

        DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
		return String.format("Hello %s ! Hôm nay %s, mua 1 tặng 1.", paramName, dtfVietnam.format(today));
    }

    
    
    

}
